﻿using Assets.Scripts;
using Grpc.Core;
using UnityEngine;
using UnityEngine.UI;

public class TestGaming : MonoBehaviour
{
    [SerializeField] private Button leaveBtn;
    [SerializeField] private InputField nameInput;
    [SerializeField] private GameObject panelJoin;
    [SerializeField] private InputField roomInput;
    [SerializeField] private float speed = 3f;

    private InputField NameInput => nameInput;

    private InputField RoomInput => roomInput;

    private GameObject PanelJoin => panelJoin;

    private Button LeaveBtn => leaveBtn;

    private float Speed => speed;

    private GamingHubClient Client { get; } = new GamingHubClient();

    private GameObject Player { get; set; }

    private Channel Channel { get; } = new Channel("localhost", 12345, ChannelCredentials.Insecure);

    // Start is called before the first frame update
    private void Start()
    {
        InitializeUi();
        LeaveBtn.onClick.AddListener(Leave);
    }

    public async void Join()
    {
        Player = await Client.ConnectAsync(Channel, RoomInput.text, NameInput.text);
        var moveRandomPosition = Player.AddComponent<MoveRandomPosition>();
        moveRandomPosition.OnMove += MoveAsync;
        PanelJoin.SetActive(false);
        LeaveBtn.gameObject.SetActive(true);
    }

    private void InitializeUi()
    {
        RoomInput.text = "MyRoom";
        PanelJoin.SetActive(true);
        LeaveBtn.gameObject.SetActive(false);
    }

    public async void Leave()
    {
        await Client.LeaveAsync();
        InitializeUi();
    }

    private async void MoveAsync(Vector3 position, Quaternion rotation)
    {
        await Client.MoveAsync(position, rotation);
    }

    private async void OnDestroy()
    {
        await Client.LeaveAsync();
        await Client.DisposeAsync();
        await Channel.ShutdownAsync();
    }
}
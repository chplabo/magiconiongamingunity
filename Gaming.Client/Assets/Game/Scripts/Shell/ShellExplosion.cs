using System;
using System.Linq;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

// ReSharper disable ConvertToAutoProperty

namespace MagicOnionCHP.Game
{
    public class ShellExplosion : MonoBehaviour
    {
        [SerializeField] private float explosionForce = 1000f;
        [SerializeField] private float explosionRadius = 5f;
        [SerializeField] private float maxDamage = 100f;
        [SerializeField] private LayerMask tankMask;
        [SerializeField] private AudioSource explosionAudio;
        [SerializeField] private ParticleSystem explosionParticles;

        private LayerMask TankMask => tankMask;

        private float MaxDamage => maxDamage;

        private float ExplosionForce => explosionForce;

        private float ExplosionRadius => explosionRadius;

        private AudioSource ExplosionAudio => explosionAudio;

        private ParticleSystem ExplosionParticles => explosionParticles;

        private void Start()
        {
            this.OnTriggerEnterAsObservable()
                .Subscribe(_ =>
                {
                    var colliders = Physics.OverlapSphere(transform.position, ExplosionRadius, TankMask);
                    var receivers = colliders.Select(col => col.GetComponent<IDamageReceiver>()).Where(x => x != null);

                    foreach (var body in receivers.Select(x => x.GetComponent<Rigidbody>()).Where(x => x != null))
                        body.AddExplosionForce(ExplosionForce, transform.position, ExplosionRadius);

                    foreach (var receiver in receivers)
                    {
                        var damage = CalculateDamage(receiver.GetPosition());
                        receiver.TakeDamage(damage);
                    }
                    
                    GetComponent<ObjectElementCommon>().ReturnPool();
                    
                    ActiveEffect();
                });
        }

        private float CalculateDamage(Vector3 targetPosition)
        {
            var explosionToTarget = targetPosition - transform.position;

            var explosionDistance = explosionToTarget.magnitude;

            var relativeDistance = (ExplosionRadius - explosionDistance) / ExplosionRadius;

            var damage = relativeDistance * MaxDamage;

            damage = Mathf.Max(0f, damage);

            return damage;
        }

        private void ActiveEffect()
        {
            ExplosionParticles.transform.parent = null;
            ExplosionParticles.gameObject.SetActive(true);
            ExplosionParticles.Play();

            var mainModule = ExplosionParticles.main;
            Observable.Timer(TimeSpan.FromSeconds(mainModule.duration))
                .Subscribe(_ =>
                {
                    ExplosionParticles.transform.SetParent(transform);
                    ExplosionParticles.transform.localPosition = Vector3.zero;
                    ExplosionParticles.gameObject.SetActive(false);
                });
            
            ExplosionAudio.Play();
        }
    }
}
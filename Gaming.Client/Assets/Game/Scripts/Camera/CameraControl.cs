using UniRx;

namespace MagicOnionCHP.Game
{
    public class CameraControl : Complete.CameraControl
    {
        private void Start()
        {
            NetworkManager.Instance.PlayerCountChanged()
                .Subscribe(_ => m_Targets = NetworkManager.Instance.GetTransformsPlayer());
        }
    }
}
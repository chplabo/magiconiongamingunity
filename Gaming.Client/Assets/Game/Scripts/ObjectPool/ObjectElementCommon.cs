using System;
using UniRx;
using UnityEngine;

namespace MagicOnionCHP.Game
{
    public class ObjectElementCommon : MonoBehaviour
    {
        [SerializeField] private float lifeTime = 5f;
        public bool IsMine { get; set; }
        
        public ObjectPool Pool { private get; set; }

        private float LifeTime => lifeTime;

        private void OnEnable()
        {
            Observable.Timer(TimeSpan.FromSeconds(LifeTime))
                .TakeUntilDisable(this)
                .Subscribe(_ => ReturnPool());
        }

        public void ReturnPool()
        {
            Pool.Return(this);
        }
    }
}
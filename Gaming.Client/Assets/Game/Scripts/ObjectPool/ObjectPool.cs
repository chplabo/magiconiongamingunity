using System;
using UniRx;
using UnityEngine;
using Object = UnityEngine.Object;

namespace MagicOnionCHP.Game
{
    public class ObjectPool : UniRx.Toolkit.AsyncObjectPool<ObjectElementCommon>
    {
        private ObjectElementCommon ObjectPrefab { get; }

        private Transform TransformParent { get; }
        
        private NetworkIdentity NetworkIdentity { get; }

        public ObjectPool(ObjectElementCommon objectPrefab, Transform transformParent, NetworkIdentity networkIdentity)
        {
            ObjectPrefab = objectPrefab;
            TransformParent = transformParent;
            NetworkIdentity = networkIdentity;
        }

        protected override IObservable<ObjectElementCommon> CreateInstanceAsync()
        {
            var obj = Object.Instantiate(ObjectPrefab, TransformParent.position, Quaternion.identity);
            obj.transform.parent = TransformParent;
            obj.IsMine = NetworkIdentity.IsMine;
            obj.Pool = this;
            return Observable.Return(obj);
        }

        protected override void OnBeforeRent(ObjectElementCommon instance)
        {
            base.OnBeforeRent(instance);
            instance.transform.parent = null;
        }

        protected override void OnBeforeReturn(ObjectElementCommon instance)
        {
            base.OnBeforeReturn(instance);
            instance.transform.parent = TransformParent;
        }
    }
}
using System;
using UnityEngine;
using UnityEngine.UI;
using UniRx;
using UniRx.Triggers;
// ReSharper disable ConvertToAutoProperty

namespace MagicOnionCHP.Game
{
    public class TankShooting : NetworkBehaviour
    {
        [SerializeField] private ObjectElementCommon shellPrefab;
        [SerializeField] private Transform fireTransform;
        [SerializeField] private Slider amiSlider;
        [SerializeField] private float minLaunchForce = 15;
        [SerializeField] private float maxLaunchForce = 30f;
        [SerializeField] private float maxChangeTime = 0.75f;
        [SerializeField] private AudioSource shootingAudio;
        [SerializeField] private AudioClip chargingClip;
        [SerializeField] private AudioClip fireClip;

        private ObjectElementCommon ShellPrefab => shellPrefab;

        private Transform FireTransform => fireTransform;

        private Slider AmiSlider => amiSlider;

        private float MinLaunchForce => minLaunchForce;

        private float MaxLaunchForce => maxLaunchForce;

        private float MaxChangeTime => maxChangeTime;

        private AudioSource ShootingAudio => shootingAudio;

        private AudioClip ChargingClip => chargingClip;

        private AudioClip FireClip => fireClip;

        private float ChangeSpeed { get; set; }

        private ObjectPool ShellPool { get; set; }

        private void Awake()
        {
            ShellPool = new ObjectPool(ShellPrefab, transform, NetworkIdentity);
            ChangeSpeed = (MaxLaunchForce - MinLaunchForce) / MaxChangeTime;
        }

        private void Start()
        {
            GameHubReceiverObserver.OnFireAsObservable()
                .Where(message => message.ViewId == NetworkIdentity.ViewID)
                .Subscribe(message => Fire(message.LaunchForce))
                .AddTo(this);
            
            if (NetworkIdentity.IsMine)
            {
                this.OnKeyDownAsObservable(KeyCode.Space)
                    .Do(_ => PlayClipChargingToFire())
                    .SelectMany(_ => Observable.EveryUpdate())
                    .Select(_ => ChangeSpeed * Time.deltaTime)
                    .StartWith(MinLaunchForce)
                    .Scan((pr, cr) => pr + cr)
                    .Do(launchForce => AmiSlider.value = launchForce)
                    .TakeWhile(launchForce => launchForce <= MaxLaunchForce)
                    .TakeUntil(this.OnKeyUpAsObservable(KeyCode.Space))
                    .Last()
                    .Where(launchForce => launchForce > MinLaunchForce)
                    .RepeatUntilDestroy(this)
                    .Subscribe(FireRpc);
            }
        }

        private void FireRpc(float launchForce)
        {
            NetworkManager.Instance.FireAsync(launchForce);
        }

        private void Fire(float launchForce)
        {
            PlayClipFire();
            
            ShellPool.RentAsync()
                .Subscribe(shell =>
                {
                    shell.transform.position = FireTransform.position;
                    shell.transform.rotation = FireTransform.rotation;
                    shell.GetComponent<Rigidbody>().velocity = launchForce * FireTransform.forward;
                });
        }

        private void PlayClipChargingToFire()
        {
            ShootingAudio.clip = ChargingClip;
            ShootingAudio.Play();
        }

        private void PlayClipFire()
        {
            ShootingAudio.clip = FireClip;
            ShootingAudio.Play();
        }
    }
}
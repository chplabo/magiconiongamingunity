using System;
using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

// ReSharper disable ConvertToAutoProperty

namespace MagicOnionCHP.Game
{
    public class TankHealth : NetworkBehaviour, IDamageReceiver
    {
        [SerializeField] private Color myHealthColor = Color.green;
        [SerializeField] private Color enemyHealthColor = Color.red;
        [SerializeField] private TextMeshProUGUI nameText;
        [SerializeField] private Image fillImage;
        [SerializeField] private Slider healthSlider;
        [SerializeField] private AudioSource explosionAudio;
        [SerializeField] private ParticleSystem explosionParticles;

        private Slider HealthSlider => healthSlider;

        private Image FillImage => fillImage;

        private Color MyHealthColor => myHealthColor;

        private Color EnemyHealthColor => enemyHealthColor;

        private TextMeshProUGUI NameText => nameText;

        private AudioSource ExplosionAudio => explosionAudio;

        private ParticleSystem ExplosionParticles => explosionParticles;

        public event Action OnDied;

        public async void TakeDamage(float amount)
        {
            if (!NetworkIdentity.IsMine) return;
            await NetworkManager.Instance.TakeDamage(amount);
        }

        Vector3 IDamageReceiver.GetPosition()
        {
            return GetComponent<Rigidbody>().position;
        }

        private void Start()
        {
            FillImage.color = NetworkIdentity.IsMine ? MyHealthColor : EnemyHealthColor;
            NameText.text = NetworkIdentity.Name;

            GameHubReceiverObserver.OnJoinAsObservable()
                .Where(player => player.ViewId == NetworkIdentity.ViewID)
                .Subscribe(player => { HealthSlider.value = player.Health; })
                .AddTo(this);

            GameHubReceiverObserver.OnTakeDamageAsObservable()
                .Where(player => player.ViewId == NetworkIdentity.ViewID)
                .Subscribe(player =>
                {
                    HealthSlider.value = player.Health;
                    if (player.Health <= 0)
                    {
                        OnDeath();
                    }
                })
                .AddTo(this);

            GameHubReceiverObserver.OnReSpawnAsObservable()
                .Where(player => player.ViewId == NetworkIdentity.ViewID)
                .Subscribe(player => { HealthSlider.value = player.Health; })
                .AddTo(this);
        }

        private void OnDeath()
        {
            ExplosionAudio.Play();

            ExplosionParticles.transform.parent = null;
            ExplosionParticles.gameObject.SetActive(true);
            ExplosionParticles.Play();

            Observable.Timer(TimeSpan.FromSeconds(2))
                .Subscribe(_ =>
                {
                    ExplosionParticles.transform.parent = transform;
                    ExplosionParticles.gameObject.SetActive(false);
                });

            gameObject.SetActive(false);
            
            if (NetworkIdentity.IsMine)
            {
                OnDied?.Invoke();
            }
        }

        private void OnDestroy()
        {
            OnDied = null;
        }
    }
}
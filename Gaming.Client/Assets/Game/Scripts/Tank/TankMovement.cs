﻿using System;
using UniRx;
using UniRx.Triggers;
using UnityEngine;
using Random = UnityEngine.Random;

// ReSharper disable ConvertToAutoProperty

namespace MagicOnionCHP.Game
{
    public class TankMovement : NetworkBehaviour
    {
        [SerializeField] private float moveSpeed = 12f;
        [SerializeField] private float turnSpeed = 180f;
       

        private float MoveSpeed => moveSpeed;

        private float TurnSpeed => turnSpeed;

        private Rigidbody Rigidbody { get; set; }

        private Vector3 Direction { get; set; } = Vector3.zero;

        private Quaternion TurnRotation { get; set; }

        private ParticleSystem[] ParticleSystems { get; set; }

        private void Awake()
        {
            Rigidbody = GetComponent<Rigidbody>();
            GetComponent<NetworkRigidbody>().Initialize(MoveSpeed, TurnSpeed);
        }

        private void OnEnable()
        {
            ParticleSystems = GetComponentsInChildren<ParticleSystem>();
            foreach (var t in ParticleSystems) t.Play();
        }

        private void Start()
        {
            
            if (!NetworkIdentity.IsMine) return;

            Observable.EveryFixedUpdate()
                .Subscribe(_ =>
                {
                    Rigidbody.MovePosition(Rigidbody.position + Direction);
                    Rigidbody.MoveRotation(Rigidbody.rotation * TurnRotation);
                })
                .AddTo(this);

            MoveDirectionStream()
                .Subscribe(input => { Direction = transform.forward * input * Time.deltaTime * MoveSpeed; });

            TurnStream()
                .Subscribe(input =>
                {
                    var turn = input * Time.deltaTime * TurnSpeed;
                    TurnRotation = Quaternion.Euler(0, turn, 0);
                });
    
        }

        private IObservable<float> MoveDirectionStream()
        {
            return this.FixedUpdateAsObservable()
                .Select(_ => Input.GetAxis("Vertical1"));
            //.DistinctUntilChanged();
        }

        private IObservable<float> TurnStream()
        {
            return this.FixedUpdateAsObservable()
                .Select(_ => Input.GetAxis("Horizontal1"));
            //.DistinctUntilChanged();
        }
    }
}
using UnityEngine;

namespace MagicOnionCHP.Game
{
    public interface IDamageReceiver
    {
        void TakeDamage(float amount);
        T GetComponent<T>();
        Vector3 GetPosition();
    }
}
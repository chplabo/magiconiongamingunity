﻿#pragma warning disable 618
#pragma warning disable 612
#pragma warning disable 414
#pragma warning disable 219
#pragma warning disable 168

namespace MagicOnion
{
    using global::System;
    using global::System.Collections.Generic;
    using global::System.Linq;
    using global::MagicOnion;
    using global::MagicOnion.Client;

    public static partial class MagicOnionInitializer
    {
        static bool isRegistered = false;

        [UnityEngine.RuntimeInitializeOnLoadMethod(UnityEngine.RuntimeInitializeLoadType.BeforeSceneLoad)]
        public static void Register()
        {
            if(isRegistered) return;
            isRegistered = true;


            StreamingHubClientRegistry<ServerShared.Hub.IGameHub, ServerShared.Hub.IGameHubReceiver>.Register((a, _, b, c, d, e) => new ServerShared.Hub.IGameHubClient(a, b, c, d, e));
            StreamingHubClientRegistry<ServerShared.Hub.IGamingHub, ServerShared.Hub.IGamingHubReceiver>.Register((a, _, b, c, d, e) => new ServerShared.Hub.IGamingHubClient(a, b, c, d, e));
        }
    }
}

#pragma warning restore 168
#pragma warning restore 219
#pragma warning restore 414
#pragma warning restore 612
#pragma warning restore 618
#pragma warning disable 618
#pragma warning disable 612
#pragma warning disable 414
#pragma warning disable 219
#pragma warning disable 168

namespace MagicOnion.Resolvers
{
    using System;
    using MessagePack;

    public class MagicOnionResolver : global::MessagePack.IFormatterResolver
    {
        public static readonly global::MessagePack.IFormatterResolver Instance = new MagicOnionResolver();

        MagicOnionResolver()
        {

        }

        public global::MessagePack.Formatters.IMessagePackFormatter<T> GetFormatter<T>()
        {
            return FormatterCache<T>.formatter;
        }

        static class FormatterCache<T>
        {
            public static readonly global::MessagePack.Formatters.IMessagePackFormatter<T> formatter;

            static FormatterCache()
            {
                var f = MagicOnionResolverGetFormatterHelper.GetFormatter(typeof(T));
                if (f != null)
                {
                    formatter = (global::MessagePack.Formatters.IMessagePackFormatter<T>)f;
                }
            }
        }
    }

    internal static class MagicOnionResolverGetFormatterHelper
    {
        static readonly global::System.Collections.Generic.Dictionary<Type, int> lookup;

        static MagicOnionResolverGetFormatterHelper()
        {
            lookup = new global::System.Collections.Generic.Dictionary<Type, int>(4)
            {
                {typeof(global::MagicOnion.DynamicArgumentTuple<global::UnityEngine.Vector3, global::UnityEngine.Quaternion>), 0 },
                {typeof(global::MagicOnion.DynamicArgumentTuple<string, string, global::UnityEngine.Vector3, global::UnityEngine.Quaternion>), 1 },
                {typeof(global::MagicOnionCHP.ServerShared.MessagePackObject.Player[]), 2 },
                {typeof(global::ServerShared.MessagePackObject.Player[]), 3 },
            };
        }

        internal static object GetFormatter(Type t)
        {
            int key;
            if (!lookup.TryGetValue(t, out key))
            {
                return null;
            }

            switch (key)
            {
                case 0: return new global::MagicOnion.DynamicArgumentTupleFormatter<global::UnityEngine.Vector3, global::UnityEngine.Quaternion>(default(global::UnityEngine.Vector3), default(global::UnityEngine.Quaternion));
                case 1: return new global::MagicOnion.DynamicArgumentTupleFormatter<string, string, global::UnityEngine.Vector3, global::UnityEngine.Quaternion>(default(string), default(string), default(global::UnityEngine.Vector3), default(global::UnityEngine.Quaternion));
                case 2: return new global::MessagePack.Formatters.ArrayFormatter<global::MagicOnionCHP.ServerShared.MessagePackObject.Player>();
                case 3: return new global::MessagePack.Formatters.ArrayFormatter<global::ServerShared.MessagePackObject.Player>();
                default: return null;
            }
        }
    }
}

#pragma warning restore 168
#pragma warning restore 219
#pragma warning restore 414
#pragma warning restore 612
#pragma warning restore 618
#pragma warning disable 618
#pragma warning disable 612
#pragma warning disable 414
#pragma warning disable 219
#pragma warning disable 168

namespace ServerShared.Hub {
    using Grpc.Core;
    using Grpc.Core.Logging;
    using MagicOnion;
    using MagicOnion.Client;
    using MessagePack;
    using System;
    using System.Threading.Tasks;

    public class IGameHubClient : StreamingHubClientBase<global::ServerShared.Hub.IGameHub, global::ServerShared.Hub.IGameHubReceiver>, global::ServerShared.Hub.IGameHub
    {
        static readonly Method<byte[], byte[]> method = new Method<byte[], byte[]>(MethodType.DuplexStreaming, "IGameHub", "Connect", MagicOnionMarshallers.ThroughMarshaller, MagicOnionMarshallers.ThroughMarshaller);

        protected override Method<byte[], byte[]> DuplexStreamingAsyncMethod { get { return method; } }

        readonly global::ServerShared.Hub.IGameHub __fireAndForgetClient;

        public IGameHubClient(CallInvoker callInvoker, string host, CallOptions option, IFormatterResolver resolver, ILogger logger)
            : base(callInvoker, host, option, resolver, logger)
        {
            this.__fireAndForgetClient = new FireAndForgetClient(this);
        }
        
        public global::ServerShared.Hub.IGameHub FireAndForget()
        {
            return __fireAndForgetClient;
        }

        protected override void OnBroadcastEvent(int methodId, ArraySegment<byte> data)
        {
            switch (methodId)
            {
                case -1297457280: // OnJoin
                {
                    var result = LZ4MessagePackSerializer.Deserialize<global::MagicOnionCHP.ServerShared.MessagePackObject.Player>(data, resolver);
                    receiver.OnJoin(result); break;
                }
                case 532410095: // OnLeave
                {
                    var result = LZ4MessagePackSerializer.Deserialize<global::MagicOnionCHP.ServerShared.MessagePackObject.Player>(data, resolver);
                    receiver.OnLeave(result); break;
                }
                case 1429874301: // OnMove
                {
                    var result = LZ4MessagePackSerializer.Deserialize<global::MagicOnionCHP.ServerShared.MessagePackObject.Player>(data, resolver);
                    receiver.OnMove(result); break;
                }
                case 1924763186: // OnMoveDirection
                {
                    var result = LZ4MessagePackSerializer.Deserialize<global::ServerShared.Struct.MoveDirectionMessage>(data, resolver);
                    receiver.OnMoveDirection(result); break;
                }
                case -1074574636: // OnFire
                {
                    var result = LZ4MessagePackSerializer.Deserialize<global::ServerShared.Struct.FireMessage>(data, resolver);
                    receiver.OnFire(result); break;
                }
                case -1436285966: // OnTakeDamage
                {
                    var result = LZ4MessagePackSerializer.Deserialize<global::MagicOnionCHP.ServerShared.MessagePackObject.Player>(data, resolver);
                    receiver.OnTakeDamage(result); break;
                }
                case -1136542310: // RequestReSpawn
                {
                    var result = LZ4MessagePackSerializer.Deserialize<global::MagicOnionCHP.ServerShared.MessagePackObject.Player>(data, resolver);
                    receiver.RequestReSpawn(result); break;
                }
                default:
                    break;
            }
        }

        protected override void OnResponseEvent(int methodId, object taskCompletionSource, ArraySegment<byte> data)
        {
            switch (methodId)
            {
                case -733403293: // JoinAsync
                {
                    var result = LZ4MessagePackSerializer.Deserialize<global::MagicOnionCHP.ServerShared.MessagePackObject.Player[]>(data, resolver);
                    ((TaskCompletionSource<global::MagicOnionCHP.ServerShared.MessagePackObject.Player[]>)taskCompletionSource).TrySetResult(result);
                    break;
                }
                case 1368362116: // LeaveAsync
                {
                    var result = LZ4MessagePackSerializer.Deserialize<Nil>(data, resolver);
                    ((TaskCompletionSource<Nil>)taskCompletionSource).TrySetResult(result);
                    break;
                }
                case -99261176: // MoveAsync
                {
                    var result = LZ4MessagePackSerializer.Deserialize<Nil>(data, resolver);
                    ((TaskCompletionSource<Nil>)taskCompletionSource).TrySetResult(result);
                    break;
                }
                case 811251167: // MoveDirectionAsync
                {
                    var result = LZ4MessagePackSerializer.Deserialize<Nil>(data, resolver);
                    ((TaskCompletionSource<Nil>)taskCompletionSource).TrySetResult(result);
                    break;
                }
                case 376964611: // FireAsync
                {
                    var result = LZ4MessagePackSerializer.Deserialize<Nil>(data, resolver);
                    ((TaskCompletionSource<Nil>)taskCompletionSource).TrySetResult(result);
                    break;
                }
                case 1099307955: // TakeDamage
                {
                    var result = LZ4MessagePackSerializer.Deserialize<Nil>(data, resolver);
                    ((TaskCompletionSource<Nil>)taskCompletionSource).TrySetResult(result);
                    break;
                }
                case -1136542310: // RequestReSpawn
                {
                    var result = LZ4MessagePackSerializer.Deserialize<Nil>(data, resolver);
                    ((TaskCompletionSource<Nil>)taskCompletionSource).TrySetResult(result);
                    break;
                }
                default:
                    break;
            }
        }
   
        public global::System.Threading.Tasks.Task<global::MagicOnionCHP.ServerShared.MessagePackObject.Player[]> JoinAsync(string roomName, string useName, global::UnityEngine.Vector3 position, global::UnityEngine.Quaternion rotation)
        {
            return WriteMessageWithResponseAsync<DynamicArgumentTuple<string, string, global::UnityEngine.Vector3, global::UnityEngine.Quaternion>, global::MagicOnionCHP.ServerShared.MessagePackObject.Player[]> (-733403293, new DynamicArgumentTuple<string, string, global::UnityEngine.Vector3, global::UnityEngine.Quaternion>(roomName, useName, position, rotation));
        }

        public global::System.Threading.Tasks.Task LeaveAsync()
        {
            return WriteMessageWithResponseAsync<Nil, Nil>(1368362116, Nil.Default);
        }

        public global::System.Threading.Tasks.Task MoveAsync(global::UnityEngine.Vector3 position, global::UnityEngine.Quaternion rotation)
        {
            return WriteMessageWithResponseAsync<DynamicArgumentTuple<global::UnityEngine.Vector3, global::UnityEngine.Quaternion>, Nil>(-99261176, new DynamicArgumentTuple<global::UnityEngine.Vector3, global::UnityEngine.Quaternion>(position, rotation));
        }

        public global::System.Threading.Tasks.Task MoveDirectionAsync(global::ServerShared.Struct.MoveDirectionMessage message)
        {
            return WriteMessageWithResponseAsync<global::ServerShared.Struct.MoveDirectionMessage, Nil>(811251167, message);
        }

        public global::System.Threading.Tasks.Task FireAsync(float launchForce)
        {
            return WriteMessageWithResponseAsync<float, Nil>(376964611, launchForce);
        }

        public global::System.Threading.Tasks.Task TakeDamage(float damage)
        {
            return WriteMessageWithResponseAsync<float, Nil>(1099307955, damage);
        }

        public global::System.Threading.Tasks.Task RequestReSpawn(global::UnityEngine.Vector3 position, global::UnityEngine.Quaternion rotation)
        {
            return WriteMessageWithResponseAsync<DynamicArgumentTuple<global::UnityEngine.Vector3, global::UnityEngine.Quaternion>, Nil>(-1136542310, new DynamicArgumentTuple<global::UnityEngine.Vector3, global::UnityEngine.Quaternion>(position, rotation));
        }


        class FireAndForgetClient : global::ServerShared.Hub.IGameHub
        {
            readonly IGameHubClient __parent;

            public FireAndForgetClient(IGameHubClient parentClient)
            {
                this.__parent = parentClient;
            }

            public global::ServerShared.Hub.IGameHub FireAndForget()
            {
                throw new NotSupportedException();
            }

            public Task DisposeAsync()
            {
                throw new NotSupportedException();
            }

            public Task WaitForDisconnect()
            {
                throw new NotSupportedException();
            }

            public global::System.Threading.Tasks.Task<global::MagicOnionCHP.ServerShared.MessagePackObject.Player[]> JoinAsync(string roomName, string useName, global::UnityEngine.Vector3 position, global::UnityEngine.Quaternion rotation)
            {
                return __parent.WriteMessageAsyncFireAndForget<DynamicArgumentTuple<string, string, global::UnityEngine.Vector3, global::UnityEngine.Quaternion>, global::MagicOnionCHP.ServerShared.MessagePackObject.Player[]> (-733403293, new DynamicArgumentTuple<string, string, global::UnityEngine.Vector3, global::UnityEngine.Quaternion>(roomName, useName, position, rotation));
            }

            public global::System.Threading.Tasks.Task LeaveAsync()
            {
                return __parent.WriteMessageAsync<Nil>(1368362116, Nil.Default);
            }

            public global::System.Threading.Tasks.Task MoveAsync(global::UnityEngine.Vector3 position, global::UnityEngine.Quaternion rotation)
            {
                return __parent.WriteMessageAsync<DynamicArgumentTuple<global::UnityEngine.Vector3, global::UnityEngine.Quaternion>>(-99261176, new DynamicArgumentTuple<global::UnityEngine.Vector3, global::UnityEngine.Quaternion>(position, rotation));
            }

            public global::System.Threading.Tasks.Task MoveDirectionAsync(global::ServerShared.Struct.MoveDirectionMessage message)
            {
                return __parent.WriteMessageAsync<global::ServerShared.Struct.MoveDirectionMessage>(811251167, message);
            }

            public global::System.Threading.Tasks.Task FireAsync(float launchForce)
            {
                return __parent.WriteMessageAsync<float>(376964611, launchForce);
            }

            public global::System.Threading.Tasks.Task TakeDamage(float damage)
            {
                return __parent.WriteMessageAsync<float>(1099307955, damage);
            }

            public global::System.Threading.Tasks.Task RequestReSpawn(global::UnityEngine.Vector3 position, global::UnityEngine.Quaternion rotation)
            {
                return __parent.WriteMessageAsync<DynamicArgumentTuple<global::UnityEngine.Vector3, global::UnityEngine.Quaternion>>(-1136542310, new DynamicArgumentTuple<global::UnityEngine.Vector3, global::UnityEngine.Quaternion>(position, rotation));
            }

        }
    }

    public class IGamingHubClient : StreamingHubClientBase<global::ServerShared.Hub.IGamingHub, global::ServerShared.Hub.IGamingHubReceiver>, global::ServerShared.Hub.IGamingHub
    {
        static readonly Method<byte[], byte[]> method = new Method<byte[], byte[]>(MethodType.DuplexStreaming, "IGamingHub", "Connect", MagicOnionMarshallers.ThroughMarshaller, MagicOnionMarshallers.ThroughMarshaller);

        protected override Method<byte[], byte[]> DuplexStreamingAsyncMethod { get { return method; } }

        readonly global::ServerShared.Hub.IGamingHub __fireAndForgetClient;

        public IGamingHubClient(CallInvoker callInvoker, string host, CallOptions option, IFormatterResolver resolver, ILogger logger)
            : base(callInvoker, host, option, resolver, logger)
        {
            this.__fireAndForgetClient = new FireAndForgetClient(this);
        }
        
        public global::ServerShared.Hub.IGamingHub FireAndForget()
        {
            return __fireAndForgetClient;
        }

        protected override void OnBroadcastEvent(int methodId, ArraySegment<byte> data)
        {
            switch (methodId)
            {
                case -1297457280: // OnJoin
                {
                    var result = LZ4MessagePackSerializer.Deserialize<global::ServerShared.MessagePackObject.Player>(data, resolver);
                    receiver.OnJoin(result); break;
                }
                case 1429874301: // OnMove
                {
                    var result = LZ4MessagePackSerializer.Deserialize<global::ServerShared.MessagePackObject.Player>(data, resolver);
                    receiver.OnMove(result); break;
                }
                case 532410095: // OnLeave
                {
                    var result = LZ4MessagePackSerializer.Deserialize<global::ServerShared.MessagePackObject.Player>(data, resolver);
                    receiver.OnLeave(result); break;
                }
                default:
                    break;
            }
        }

        protected override void OnResponseEvent(int methodId, object taskCompletionSource, ArraySegment<byte> data)
        {
            switch (methodId)
            {
                case -733403293: // JoinAsync
                {
                    var result = LZ4MessagePackSerializer.Deserialize<global::ServerShared.MessagePackObject.Player[]>(data, resolver);
                    ((TaskCompletionSource<global::ServerShared.MessagePackObject.Player[]>)taskCompletionSource).TrySetResult(result);
                    break;
                }
                case 1368362116: // LeaveAsync
                {
                    var result = LZ4MessagePackSerializer.Deserialize<Nil>(data, resolver);
                    ((TaskCompletionSource<Nil>)taskCompletionSource).TrySetResult(result);
                    break;
                }
                case -99261176: // MoveAsync
                {
                    var result = LZ4MessagePackSerializer.Deserialize<Nil>(data, resolver);
                    ((TaskCompletionSource<Nil>)taskCompletionSource).TrySetResult(result);
                    break;
                }
                default:
                    break;
            }
        }
   
        public global::System.Threading.Tasks.Task<global::ServerShared.MessagePackObject.Player[]> JoinAsync(string roomName, string useName, global::UnityEngine.Vector3 position, global::UnityEngine.Quaternion rotation)
        {
            return WriteMessageWithResponseAsync<DynamicArgumentTuple<string, string, global::UnityEngine.Vector3, global::UnityEngine.Quaternion>, global::ServerShared.MessagePackObject.Player[]> (-733403293, new DynamicArgumentTuple<string, string, global::UnityEngine.Vector3, global::UnityEngine.Quaternion>(roomName, useName, position, rotation));
        }

        public global::System.Threading.Tasks.Task LeaveAsync()
        {
            return WriteMessageWithResponseAsync<Nil, Nil>(1368362116, Nil.Default);
        }

        public global::System.Threading.Tasks.Task MoveAsync(global::UnityEngine.Vector3 position, global::UnityEngine.Quaternion rotation)
        {
            return WriteMessageWithResponseAsync<DynamicArgumentTuple<global::UnityEngine.Vector3, global::UnityEngine.Quaternion>, Nil>(-99261176, new DynamicArgumentTuple<global::UnityEngine.Vector3, global::UnityEngine.Quaternion>(position, rotation));
        }


        class FireAndForgetClient : global::ServerShared.Hub.IGamingHub
        {
            readonly IGamingHubClient __parent;

            public FireAndForgetClient(IGamingHubClient parentClient)
            {
                this.__parent = parentClient;
            }

            public global::ServerShared.Hub.IGamingHub FireAndForget()
            {
                throw new NotSupportedException();
            }

            public Task DisposeAsync()
            {
                throw new NotSupportedException();
            }

            public Task WaitForDisconnect()
            {
                throw new NotSupportedException();
            }

            public global::System.Threading.Tasks.Task<global::ServerShared.MessagePackObject.Player[]> JoinAsync(string roomName, string useName, global::UnityEngine.Vector3 position, global::UnityEngine.Quaternion rotation)
            {
                return __parent.WriteMessageAsyncFireAndForget<DynamicArgumentTuple<string, string, global::UnityEngine.Vector3, global::UnityEngine.Quaternion>, global::ServerShared.MessagePackObject.Player[]> (-733403293, new DynamicArgumentTuple<string, string, global::UnityEngine.Vector3, global::UnityEngine.Quaternion>(roomName, useName, position, rotation));
            }

            public global::System.Threading.Tasks.Task LeaveAsync()
            {
                return __parent.WriteMessageAsync<Nil>(1368362116, Nil.Default);
            }

            public global::System.Threading.Tasks.Task MoveAsync(global::UnityEngine.Vector3 position, global::UnityEngine.Quaternion rotation)
            {
                return __parent.WriteMessageAsync<DynamicArgumentTuple<global::UnityEngine.Vector3, global::UnityEngine.Quaternion>>(-99261176, new DynamicArgumentTuple<global::UnityEngine.Vector3, global::UnityEngine.Quaternion>(position, rotation));
            }

        }
    }
}

#pragma warning restore 168
#pragma warning restore 219
#pragma warning restore 414
#pragma warning restore 618
#pragma warning restore 612

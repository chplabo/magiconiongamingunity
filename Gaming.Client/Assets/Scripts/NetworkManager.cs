using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using MagicOnion.Client;
using MagicOnionCHP.ServerShared.MessagePackObject;
using ServerShared.Hub;
using ServerShared.Struct;
using UniRx;
using UniRx.Async;
using UnityEngine;

// ReSharper disable All

namespace MagicOnionCHP
{
    public class NetworkManager : MonoBehaviour
    {
        private const string NetworkSettingName = "NetworkSetting";
        private static NetworkManager instance;

        [SerializeField] private NetworkIdentity playerPrefab;

        private NetworkIdentity PlayerPrefab => playerPrefab;

        private IGameHub Client { get; set; }

        private GameHubReceiver GameHubReceiver { get; } = new GameHubReceiver();

        private Channel GrpcChanel { get; set; }

        private ReactiveDictionary<int, GameObject> PlayerMap { get; } = new ReactiveDictionary<int, GameObject>();

        private string PlayerName { get; set; }

        private Player Self { get; set; }

        private NetworkSetting NetworkSetting { get; set; }

        public static NetworkManager Instance => instance;

        public Transform[] GetTransformsPlayer()
        {
            return PlayerMap.Values.Select(x => x.transform).ToArray();
        }

        public IObservable<int> PlayerCountChanged()
        {
            return PlayerMap.ObserveCountChanged();
        }

        private void Awake()
        {
            instance = this;
            NetworkSetting = Resources.Load<NetworkSetting>(NetworkSettingName);
            GrpcChanel = NetworkSetting.GetChanel();
        }

        private void Start()
        {
            GameHubReceiver.OnJoinAsObservable()
                .Subscribe(player =>
                {
                    Debug.Log($"Join Player : {player.Name} {player.ViewId}");

                    var identity = Instantiate(PlayerPrefab, player.Position, player.Rotation);
                    identity.name = player.Name;
                    identity.ViewID = player.ViewId;
                    ((IInitialize<IGameHubReceiverObserver>) identity).Initialize(GameHubReceiver);

                    PlayerMap[player.ViewId] = identity.gameObject;

                    if (player.Name == PlayerName)
                        identity.IsMine = true;
                });

            GameHubReceiver.OnLeaveAsObservable()
                .Subscribe(player =>
                {
                    Debug.Log($"Leave Player: {player.Name} {player.ViewId}");

                    if (PlayerMap.TryGetValue(player.ViewId, out var obj))
                    {
                        Destroy(obj);
                        PlayerMap.Remove(player.ViewId);
                    }
                });
        }

        public async Task<GameObject> ConnectAsync(string roomName, string playerName,
            Vector3 posSpawn = default, Quaternion rotation = default)
        {
            PlayerName = playerName;
            Client = StreamingHubClient.Connect<IGameHub, IGameHubReceiver>(GrpcChanel, GameHubReceiver);
            ClearGameObject();
            var roomPlayers = await Client.JoinAsync(roomName, playerName, posSpawn, rotation);

            foreach (var player in roomPlayers)
            {
                if (player.Name == playerName)
                {
                    Self = player;
                    continue;
                }

                (GameHubReceiver as IGameHubReceiver).OnJoin(player);
            }

            return PlayerMap[Self.ViewId];
        }

        public async Task LeaveAsync()
        {
            await Client.LeaveAsync();
            await Client.DisposeAsync();
        }

        public Task MoveAsync(Vector3 position, Quaternion rotation)
        {
            return Client.MoveAsync(position, rotation);
        }

        public Task MoveDirectionAsync(MoveDirectionMessage message)
        {
            return Client.MoveDirectionAsync(message);
        }

        public Task FireAsync(float launchForce)
        {
            return Client.FireAsync(launchForce);
        }

        public Task TakeDamage(float amount)
        {
            return Client.TakeDamage(amount);
        }

        public Task RequestReSpawn(Vector3 position, Quaternion rotation)
        {
            return Client.RequestReSpawn(position, rotation);
        }

        public IObservable<GameObject> OnReSpawnObject()
        {
            return GameHubReceiver.OnReSpawnAsObservable()
                .Do(player =>
                {
                    var obj = PlayerMap[player.ViewId];
                    obj.transform.SetPositionAndRotation(player.Position, player.Rotation);
                })
                .Select(player => PlayerMap[player.ViewId]);
        }

        private void ClearGameObject()
        {
            foreach (var playerMapValue in PlayerMap.Values)
                Destroy(playerMapValue);
            
            PlayerMap.Clear();
        }
        
        private async void OnDestroy()
        {
            await GrpcChanel?.ShutdownAsync();
            if (Client != null)
            {
                await LeaveAsync();
            }
        }
    }
}
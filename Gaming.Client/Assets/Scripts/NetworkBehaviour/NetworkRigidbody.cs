using System;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

namespace MagicOnionCHP
{
    [RequireComponent(typeof(Rigidbody))]
    public class NetworkRigidbody : NetworkBehaviour
    {
        private float MoveSpeed { get; set; } = 1.0f;

        private float TurnSpeed { get; set; } = 1.0f;

        private Rigidbody Rigidbody { get; set; }

        private Vector3 NetworkPosition { get; set; }
        private Quaternion NetworkRotation { get; set; }

        public void Initialize(float moveSpeed, float turnSpeed)
        {
            MoveSpeed = moveSpeed;
            TurnSpeed = turnSpeed;
        }

        private void Awake()
        {
            Rigidbody = GetComponent<Rigidbody>();
        }

        private void Start()
        {
            if (NetworkIdentity.IsMine)
            {
                this.Rigidbody.ObserveEveryValueChanged(x => x.position).AsUnitObservable()
                    .Merge(this.Rigidbody.ObserveEveryValueChanged(x => x.rotation).AsUnitObservable())
                    .ThrottleFirst(TimeSpan.FromMilliseconds(100))
                    .Subscribe(_ => NetworkManager.Instance.MoveAsync(Rigidbody.position, Rigidbody.rotation))
                    .AddTo(this);
            }
            else
            {
                NetworkPosition = Rigidbody.position;
                NetworkRotation = Rigidbody.rotation;
                
                this.FixedUpdateAsObservable()
                    .Subscribe(_ =>
                    {
                        Rigidbody.position = Vector3.MoveTowards(Rigidbody.position, NetworkPosition,
                            Time.deltaTime * MoveSpeed);
                        Rigidbody.rotation = Quaternion.RotateTowards(Rigidbody.rotation, NetworkRotation,
                            Time.deltaTime * TurnSpeed);
                    }).AddTo(this);

                this.GameHubReceiverObserver
                    .OnMoveAsObservable()
                    .Where(player => player.ViewId == NetworkIdentity.ViewID)
                    .Subscribe(player =>
                    {
                        NetworkPosition = player.Position;
                        NetworkRotation = player.Rotation;
                    }).AddTo(this);
            }
        }
    }
}
using UnityEngine;

namespace MagicOnionCHP
{
    public class NetworkBehaviour : MonoBehaviour
    {
        private NetworkIdentity networkIdentityCache;

        protected NetworkIdentity NetworkIdentity => networkIdentityCache
            ? networkIdentityCache
            : networkIdentityCache = GetComponent<NetworkIdentity>();

        protected IGameHubReceiverObserver GameHubReceiverObserver => NetworkIdentity.GameHubReceiverObserver;
    }
}
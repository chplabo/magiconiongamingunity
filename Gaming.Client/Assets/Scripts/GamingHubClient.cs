using System.Collections.Generic;
using System.Threading.Tasks;
using Grpc.Core;
using MagicOnion.Client;
using ServerShared.Hub;
using ServerShared.MessagePackObject;
using UniRx;
using UnityEngine;

namespace Assets.Scripts
{
    public class GamingHubClient : IGamingHubReceiver
    {
        private Dictionary<string, GameObject> Players { get; } = new Dictionary<string, GameObject>();

        private Dictionary<string, Vector3> PlayersPosition { get; } = new Dictionary<string, Vector3>();

        private IGamingHub Client { get; set; }

        private Player Self { get; set; }

        private string MyName { get; set; }

        public async Task<GameObject> ConnectAsync(Channel grpcChanel, string roomName, string playerName)
        {
            Client = StreamingHubClient.Connect<IGamingHub, IGamingHubReceiver>(grpcChanel, this);
            MyName = playerName;
            var roomPlayers = await Client.JoinAsync(roomName, playerName, Vector3.zero, Quaternion.identity);
            foreach (var player in roomPlayers)
            {
                if (player.Name == playerName)
                {
                    Self = player;
                    continue;
                }

                (this as IGamingHubReceiver).OnJoin(player);
            }

            return Players[playerName];
        }

        public Task LeaveAsync()
        {
            return Client.LeaveAsync();
        }

        public Task MoveAsync(Vector3 position, Quaternion rotation)
        {
            return Client.MoveAsync(position, rotation);
        }

        void IGamingHubReceiver.OnJoin(Player player)
        {
            Debug.Log("Join Player:" + player.Name);

            var cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube.name = player.Name;
            cube.transform.SetPositionAndRotation(player.Position, player.Rotation);
            Players[player.Name] = cube;
            PlayersPosition[player.Name] = cube.transform.position;

            if (MyName == player.Name)
            {
                return;
            }

            Observable.EveryUpdate()
                .TakeUntilDestroy(cube)
                .Subscribe(_ =>
                {
                    cube.transform.position = Vector3.MoveTowards(cube.transform.position,
                        PlayersPosition[player.Name], Time.deltaTime * 3);
                });
        }

        void IGamingHubReceiver.OnLeave(Player player)
        {
            Debug.Log("Leave Player:" + player.Name);

            if (Players.TryGetValue(player.Name, out var cube))
            {
                GameObject.Destroy(cube);
            }

            PlayersPosition.Remove(player.Name);
        }

        void IGamingHubReceiver.OnMove(Player player)
        {
            Debug.Log("Move Player:" + player.Name);

            if (PlayersPosition.ContainsKey(player.Name))
            {
                PlayersPosition[player.Name] = player.Position;
            }

//            if (Players.TryGetValue(player.Name, out var cube))
//            {
//                //cube.transform.SetPositionAndRotation(player.Position, player.Rotation);
//            }
        }

        public Task DisposeAsync()
        {
            return Client.DisposeAsync();
        }
    }
}
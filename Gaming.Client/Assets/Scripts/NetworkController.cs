using System;
using MagicOnionCHP.Game;
using ServerShared.MessagePackObject;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;
// ReSharper disable ConvertToAutoProperty

namespace MagicOnionCHP
{
    public class NetworkController : MonoBehaviour
    {
        private const float TimerRespawn = 3f;

        [SerializeField] private InputField nameInput;
        [SerializeField] private InputField roomInput;
        [SerializeField] private Button joinButton;
        [SerializeField] private Button leaveButton;
        [SerializeField] private GameObject lobbyPanel;
        [SerializeField] private GameObject waitPanel;
        [SerializeField] private Text messageText;


        [Header("Spawn Object")] [SerializeField]
        private float rangeSpawn = 2;

        [SerializeField] private Transform[] pointsSpawn;

        private InputField NameInput => nameInput;

        private InputField RoomInput => roomInput;

        private Button JoinButton => joinButton;

        private Button LeaveButton => leaveButton;

        private GameObject LobbyPanel => lobbyPanel;

        private GameObject WaitPanel => waitPanel;

        private Text MessageText => messageText;

        private float RangeSpawn => rangeSpawn;

        private Transform[] PointsSpawn => pointsSpawn;

        private GameObject Player { get; set; }

        private void Start()
        {
            LeaveButton.onClick.AddListener(LeaveAsync);
            InitializeUi();

            NetworkManager.Instance.OnReSpawnObject()
                .Subscribe(obj =>
                {
                    if (obj.name == NameInput.text)
                    {
                        //Show count time
                        WaitPanel.SetActive(true);      
                        Observable.EveryUpdate()
                            .Select(_ => 1f)
                            .StartWith(TimerRespawn)
                            .ThrottleFirst(TimeSpan.FromSeconds(1))
                            .Scan((pr, cr) => pr - cr)
                            .Do(time => MessageText.text = time.ToString())
                            .TakeWhile(time => time > 0)
                            .Last()
                            .Where(_ => obj != null && WaitPanel.activeSelf)
                            .Subscribe(_ =>
                            {
                                obj.SetActive(true);
                                WaitPanel.SetActive(false);
                            });
                    }
                    else
                    {
                        Observable.Timer(TimeSpan.FromSeconds(TimerRespawn))
                            .Subscribe(_ => obj.SetActive(true));
                    }
                });
        }

        private void InitializeUi()
        {
            RoomInput.text = (string.IsNullOrEmpty(RoomInput.text)) ? "CommonRoom" : RoomInput.text;
            LeaveButton.gameObject.SetActive(false);
            LobbyPanel.SetActive(true);
            WaitPanel.SetActive(false);

            JoinButton.OnClickAsObservable()
                .First()
                .Subscribe(_ => JoinAsync());
        }

        public async void JoinAsync()
        {
            Player = await NetworkManager.Instance.ConnectAsync(RoomInput.text, NameInput.text, GetRandomPointSpawn(),
                Quaternion.identity);

            Player.GetComponent<TankHealth>().OnDied += () =>
                NetworkManager.Instance.RequestReSpawn(GetRandomPointSpawn(), Quaternion.identity);

            LobbyPanel.SetActive(false);
            LeaveButton.gameObject.SetActive(true);
        }

        public async void LeaveAsync()
        {
            await NetworkManager.Instance.LeaveAsync();
            InitializeUi();
        }

        private Vector3 GetRandomPointSpawn()
        {
            var indexRandom = UnityEngine.Random.Range(0, PointsSpawn.Length);
            var pointRandomTemp = Random.insideUnitSphere * RangeSpawn;
            return PointsSpawn[indexRandom].position + new Vector3(pointRandomTemp.x, 0, pointRandomTemp.y);
        }
    }
}
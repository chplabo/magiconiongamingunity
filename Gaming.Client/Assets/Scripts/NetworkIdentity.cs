using System;
using MagicOnionCHP.ServerShared.MessagePackObject;
using ServerShared.Hub;
using ServerShared.Struct;
using UniRx;
using UnityEngine;

namespace MagicOnionCHP
{
    public class NetworkIdentity : MonoBehaviour,
        IInitialize<IGameHubReceiverObserver>
    {
        public bool IsMine { get; set; } = false;

        public string Name => gameObject.name;
        
        public int ViewID { get; set; }
        
        public IGameHubReceiverObserver GameHubReceiverObserver { get; private set; }

        void IInitialize<IGameHubReceiverObserver>.Initialize(IGameHubReceiverObserver init)
        {
            GameHubReceiverObserver = init;
        }
    }
}
using System;
using MagicOnionCHP.ServerShared.MessagePackObject;
using ServerShared.Hub;
using ServerShared.Struct;
using UniRx;

namespace MagicOnionCHP
{
    public class GameHubReceiver : 
        IGameHubReceiver,
        IGameHubReceiverObserver
    {
        
        private ISubject<Player> OnJoinSubject { get; } = new ReplaySubject<Player>();
        private ISubject<Player> OnLeaveSubject { get; } = new ReplaySubject<Player>();
        private ISubject<Player> OnMoveSubject { get; } = new Subject<Player>();
        private ISubject<MoveDirectionMessage> OnMoveDirectionSubject { get; } = new Subject<MoveDirectionMessage>();
        private ISubject<FireMessage> OnFireSubject { get; } = new Subject<FireMessage>();
        private ISubject<Player> OnTakeDamageSubject { get; } = new Subject<Player>();
        private ISubject<Player> OnReSpawnSubject { get; } = new Subject<Player>();

        public IObservable<Player> OnJoinAsObservable() => OnJoinSubject;
        public IObservable<Player> OnLeaveAsObservable() => OnLeaveSubject;
        public IObservable<Player> OnMoveAsObservable() => OnMoveSubject;
        public IObservable<MoveDirectionMessage> OnMoveDirectionAsObservable() => OnMoveDirectionSubject;
        public IObservable<FireMessage> OnFireAsObservable() => OnFireSubject;
        public IObservable<Player> OnTakeDamageAsObservable() => OnTakeDamageSubject;
        public IObservable<Player> OnReSpawnAsObservable() => OnReSpawnSubject;

        void IGameHubReceiver.OnJoin(Player player)
        {
            OnJoinSubject.OnNext(player);
        }

        void IGameHubReceiver.OnLeave(Player player)
        {
            OnLeaveSubject.OnNext(player);
        }

        void IGameHubReceiver.OnMove(Player player)
        {
            OnMoveSubject.OnNext(player);
        }

        void IGameHubReceiver.OnMoveDirection(MoveDirectionMessage message)
        {
            OnMoveDirectionSubject.OnNext(message);
        }

        void IGameHubReceiver.OnFire(FireMessage fireMessage)
        {
            OnFireSubject.OnNext(fireMessage);
        }

        void IGameHubReceiver.OnTakeDamage(Player player)
        {
            OnTakeDamageSubject.OnNext(player);
        }

        public void RequestReSpawn(Player player)
        {
            OnReSpawnSubject.OnNext(player);
        }
    }
}
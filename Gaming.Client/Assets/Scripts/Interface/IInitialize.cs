namespace MagicOnionCHP
{
    public interface IInitialize<in T>
    {
        void Initialize(T init);
    }
}
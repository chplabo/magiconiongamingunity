using System;
using MagicOnionCHP.ServerShared.MessagePackObject;
using ServerShared.Struct;

namespace MagicOnionCHP
{
    public interface IGameHubReceiverObserver
    {
        IObservable<Player> OnJoinAsObservable();
        IObservable<Player> OnLeaveAsObservable();
        IObservable<Player> OnMoveAsObservable();
        IObservable<MoveDirectionMessage> OnMoveDirectionAsObservable();
        IObservable<FireMessage> OnFireAsObservable();
        IObservable<Player> OnTakeDamageAsObservable();
        IObservable<Player> OnReSpawnAsObservable();
    }
}
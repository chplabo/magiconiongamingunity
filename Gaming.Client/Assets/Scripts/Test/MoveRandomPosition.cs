﻿using System;
using UniRx;
using UnityEngine;
using Random = UnityEngine.Random;

public class MoveRandomPosition : MonoBehaviour
{
    [field: SerializeField] private float Range { get; } = 3;


    [field: SerializeField] private float Speed { get; } = 3;

    private Vector3 CurrentDestination { get; set; } = Vector3.zero;
    
    private Vector3 PositionInstance { get; set; }

    public event Action<Vector3, Quaternion> OnMove;

    // Start is called before the first frame update
    private void Start()
    {
        PositionInstance = transform.position;
        
        Observable.EveryUpdate()
            .Select(_ => Vector3.Distance(PositionInstance, CurrentDestination) <= float.Epsilon)
            .Subscribe(b =>
            {
                if (b)
                    RandomDestination();
                else
                {                    
                    PositionInstance = Vector3.MoveTowards(PositionInstance, CurrentDestination, Time.deltaTime * Speed);
                    transform.position = PositionInstance;
                }
            }).AddTo(this);

        Observable.EveryUpdate()
            .ThrottleFirst(TimeSpan.FromMilliseconds(50))
            .Select(_ => PositionInstance)
            .DistinctUntilChanged()
            .Subscribe(x => OnMoveInvoker(x, Quaternion.identity));
    }

    private void RandomDestination()
    {
        CurrentDestination = Random.insideUnitCircle * Range;
    }

    protected virtual void OnMoveInvoker(Vector3 arg1, Quaternion arg2)
    {
        OnMove?.Invoke(arg1, arg2);
    }
}
using MessagePack;
using UnityEngine;

namespace ServerShared.MessagePackObject
{
    [MessagePackObject]
    public class Player
    {
        [Key(0)]
        public string Name { get; set; }
        
        [Key(1)]
        public Vector3 Position { get; set; }
        
        [Key(2)]
        public Quaternion Rotation { get; set; }
    }
}

namespace MagicOnionCHP.ServerShared.MessagePackObject
{
    [MessagePackObject]
    public class Player
    {
        [Key(0)]
        public int ViewId { get; }

        [Key(1)]
        public string Name { get; }

        [Key(2)]
        public Vector3 Position { get; set; }

        [Key(3)]
        public Quaternion Rotation { get; set; }

        [Key(4)] 
        public float Health { get; set; } = 100.0f;

        public Player(int viewId, string name, Vector3 position, Quaternion rotation) : this()
        {
            ViewId = viewId;
            Name = name;
            Position = position;
            Rotation = rotation;
        }

        public Player()
        {
        }
    }
}
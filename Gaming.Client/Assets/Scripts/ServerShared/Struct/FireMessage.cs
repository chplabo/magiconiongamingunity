using MessagePack;

namespace ServerShared.Struct
{
    [MessagePackObject]
    public struct FireMessage
    {
        [Key(0)]
        public int ViewId { get; }
        [Key(1)]
        public float LaunchForce { get; }

        public FireMessage(int viewId, float launchForce)
        {
            ViewId = viewId;
            LaunchForce = launchForce;
        }
    }
}
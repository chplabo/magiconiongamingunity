using MessagePack;
using UnityEngine;

namespace ServerShared.Struct
{
    [MessagePackObject]
    public struct MoveDirectionMessage
    {
        [Key(0)]
        public int PlayerView { get;}
        [Key(1)]
        public float MoveSpeed { get;}
        [Key(2)]
        public float TurnSpeed { get;}

        public MoveDirectionMessage(float moveSpeed, float turnSpeed) : this()
        {
            MoveSpeed = moveSpeed;
            TurnSpeed = turnSpeed;
        }

        public MoveDirectionMessage(int playerView, float moveSpeed, float turnSpeed)
        {
            PlayerView = playerView;
            MoveSpeed = moveSpeed;
            TurnSpeed = turnSpeed;
        }
    }
}
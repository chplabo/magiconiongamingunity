using System.Threading.Tasks;
using MagicOnion;
using ServerShared.MessagePackObject;
using UnityEngine;

namespace ServerShared.Hub
{
    public interface IGamingHub : IStreamingHub<IGamingHub, IGamingHubReceiver>
    {
        Task<Player[]> JoinAsync(string roomName, string useName, Vector3 position, Quaternion rotation);
        Task LeaveAsync();
        Task MoveAsync(Vector3 position, Quaternion rotation);
    }
}
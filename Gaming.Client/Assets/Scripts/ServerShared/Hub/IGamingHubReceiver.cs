using System.Threading.Tasks;
using ServerShared.MessagePackObject;

namespace ServerShared.Hub
{
    public interface IGamingHubReceiver
    {
        void OnJoin(Player player);
        void OnMove(Player player);
        void OnLeave(Player player);
    }
}
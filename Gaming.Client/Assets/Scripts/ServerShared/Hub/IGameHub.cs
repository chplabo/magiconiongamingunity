using System.Threading.Tasks;
using MagicOnion;
using MagicOnionCHP.ServerShared.MessagePackObject;
using ServerShared.Struct;
using UnityEngine;

namespace ServerShared.Hub
{
    public interface IGameHub : IStreamingHub<IGameHub, IGameHubReceiver>
    {
        Task<Player[]> JoinAsync(string roomName, string useName, Vector3 position, Quaternion rotation);
        Task LeaveAsync();
        Task MoveAsync(Vector3 position, Quaternion rotation);
        Task MoveDirectionAsync(MoveDirectionMessage message);
        Task FireAsync(float launchForce);
        Task TakeDamage(float damage);
        Task RequestReSpawn(Vector3 position, Quaternion rotation);
    }
}

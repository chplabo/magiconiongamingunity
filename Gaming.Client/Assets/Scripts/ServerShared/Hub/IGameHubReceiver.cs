using MagicOnionCHP.ServerShared.MessagePackObject;
using ServerShared.Struct;

namespace ServerShared.Hub
{
    public interface IGameHubReceiver
    {
        void OnJoin(Player player);
        void OnLeave(Player player);
        void OnMove(Player player);
        void OnMoveDirection(MoveDirectionMessage message);
        void OnFire(FireMessage fireMessage);
        void OnTakeDamage(Player player);
        void RequestReSpawn(Player player);
    }
}
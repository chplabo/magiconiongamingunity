using Grpc.Core;
using UnityEditor;
using UnityEngine;

namespace MagicOnionCHP
{
    [CreateAssetMenu(fileName = "NetworkSetting", menuName = "MagicOnionCHP/Setting", order = 1)]
    public class NetworkSetting : ScriptableObject
    {
        [SerializeField] private string host = "localhost";
        [SerializeField] private int port = 8888;

        public string Host => host;

        public int Port => port;

        public Channel GetChanel()
        {
            return new Channel(Host, Port, ChannelCredentials.Insecure);
        }
    }
}
﻿using MagicOnion;
using MagicOnion.Resolvers;
using MessagePack.Resolvers;
using MessagePack.Unity;
using MessagePack.Unity.Extension;
using ServerShared.Struct;
using UnityEngine;

internal class InitialSettings
{
    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    private static void RegisterZeroDeserializationMapping()
    {
        UnsafeDirectBlitResolver.Register<MoveDirectionMessage>();
    }

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    private static void RegisterResolvers()
    {
        CompositeResolver.RegisterAndSetAsDefault
        (
            MagicOnionResolver.Instance,
            CompositeResolver.Instance,
            GeneratedResolver.Instance,
            BuiltinResolver.Instance,
            PrimitiveObjectResolver.Instance,
            UnsafeDirectBlitResolver.Instance,
            UnityBlitResolver.Instance,
            UnityResolver.Instance
            //StandardResolver.Instance
        );
    }
}
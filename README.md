# MagicOnionGamingUnity

## What

* Demo game use [MagicOnion](https://github.com/Cysharp/MagicOnion)

## Requirement

* UniRx
* .Net 4.x
* MagicOnion

## Install

```shell
yarn 
```
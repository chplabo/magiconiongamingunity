using System;
using System.Linq;
using System.Threading.Tasks;
using MagicOnion.Server.Hubs;
using MagicOnionCHP.ServerShared.MessagePackObject;
using ServerShared.Hub;
using ServerShared.Struct;
using UnityEngine;

namespace Gaming.Server
{
    public class GameHubStreaming : StreamingHubBase<IGameHub, IGameHubReceiver>, IGameHub
    {
        private IGroup Room { get; set; }
        private Player Self { get; set; }
        private IInMemoryStorage<Player> Storage { get; set; }

        public async Task<Player[]> JoinAsync(string roomName, string useName, Vector3 position, Quaternion rotation)
        {
            var viewId = (int) (DateTime.Now.Ticks / 10) % 1000000000;
            Self = new Player(viewId, useName, position, rotation);

            (Room, Storage) = await Group.AddAsync(roomName, Self);

            Broadcast(Room).OnJoin(Self);

            return Storage.AllValues.ToArray();
        }

        public async Task LeaveAsync()
        {
            this.Broadcast(Room).OnLeave(Self);
            await Room.RemoveAsync(this.Context);
        }

        public async Task MoveAsync(Vector3 position, Quaternion rotation)
        {
            Self.Position = position;
            Self.Rotation = rotation;
            BroadcastExceptSelf(Room).OnMove(Self);
        }

        protected override async ValueTask OnDisconnected()
        {
            if (Room != null)
            {
                await Room.RemoveAsync(this.Context);
            }
        }

        public async Task MoveDirectionAsync(MoveDirectionMessage message)
        {
            Broadcast(Room)
                .OnMoveDirection(new MoveDirectionMessage(Self.ViewId, message.MoveSpeed, message.TurnSpeed));
        }

        public async Task FireAsync(float launchForce)
        {
            Broadcast(Room).OnFire(new FireMessage(Self.ViewId, launchForce));
        }

        public async Task TakeDamage(float damage)
        {
            if (Self.Health <= 0) return;
            Self.Health -= damage;
            Broadcast(Room).OnTakeDamage(Self);

        }

        public async Task RequestReSpawn(Vector3 position, Quaternion rotation)
        {
            Self.Health = 100;
            Self.Position = position;
            Self.Rotation = rotation;
            Broadcast(Room).RequestReSpawn(Self);
        }
    }
}
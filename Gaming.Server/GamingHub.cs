using System.Linq;
using System.Threading.Tasks;
using MagicOnion.Server.Hubs;
using ServerShared.Hub;
using ServerShared.MessagePackObject;
using UnityEngine;

namespace Gaming.Server
{
//    public class GamingHub : StreamingHubBase<IGamingHub, IGamingHubReceiver>, IGamingHub
//    {
//        private IGroup Room { get; set; }
//        private Player Self { get; set; }
//        private IInMemoryStorage<Player> Storage { get; set; }
//        
//        public async Task<Player[]> JoinAsync(string roomName, string useName, Vector3 position, Quaternion rotation)
//        {
//            Self = new Player() {Name = useName, Position = position, Rotation = rotation};
//
//            (Room, Storage) = await Group.AddAsync(roomName, Self);
//
//            Broadcast(Room).OnJoin(Self);
//
//            return Storage.AllValues.ToArray();
//        }
//
//        public async Task LeaveAsync()
//        {
//            this.Broadcast(Room).OnLeave(Self);
//            await Room.RemoveAsync(this.Context);
//        }
//
//        public async Task MoveAsync(Vector3 position, Quaternion rotation)
//        {
//            Self.Position = position;
//            Self.Rotation = rotation;
//            Broadcast(Room).OnMove(Self);
//        }
//
//        protected override async ValueTask OnDisconnected()
//        {
//            if (Room != null)
//            {
//                await Room.RemoveAsync(this.Context);
//            }
//        }
//    }
}
﻿using System;
using System.Threading.Tasks;
using Grpc.Core;
using MagicOnion.Hosting;
using MagicOnion.Server;
using Microsoft.Extensions.Hosting;

namespace Gaming.Server
{
    class Program
    {
        static async Task Main(string[] args)
        {
            GrpcEnvironment.SetLogger(new Grpc.Core.Logging.ConsoleLogger());

            await new HostBuilder()
                .UseMagicOnion(new[]
                    {
                        new ServerPort("0.0.0.0", 50000, ServerCredentials.Insecure)
                    },
                    new MagicOnionOptions() {IsReturnExceptionStackTraceInErrorDetail = true}
                ).RunConsoleAsync();
        }
    }
}